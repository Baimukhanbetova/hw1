﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    public partial class Search : Form
    {
        public Search()
        {
            InitializeComponent();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            DialogResult dialogresult = folderBrowserDialog.ShowDialog();

            if (dialogresult == DialogResult.OK)
            {
                string folder = folderBrowserDialog.SelectedPath;
                string[] files1 = Directory.GetFiles(folder, searchTextBox.Text);
                for (int i = 0; i < files1.Length; i++)
                {
                    showListBox.Items.Add(files1[i]);
                }
            }
        }
    }
}

